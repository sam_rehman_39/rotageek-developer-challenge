import {
  SUBMITTING_FORM,
  SUBMIT_FORM_SUCCESS,
  SUBMIT_FORM_ERROR
} from "../actions/types";

interface IForm {
  isFetching: boolean;
  error: boolean;
  values: { name: string; email: string; phone: string; message: string };
}

const initialState: IForm = {
  isFetching: false,
  error: false,
  values: { name: "", email: "", phone: "", message: "" }
};

export default function(
  state = initialState,
  action: { type: string; payload: {} }
) {
  switch (action.type) {
    case SUBMITTING_FORM:
    case SUBMIT_FORM_SUCCESS:
    case SUBMIT_FORM_ERROR:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
