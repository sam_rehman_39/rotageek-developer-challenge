import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Header from "./Header/Header";
import Landing from "./Landing/Landing";
import ContactUs from "./ContactUs/ContactUs";

const App = () => (
  <div>
    <BrowserRouter>
      <div className="bg-light">
        <Header />
        <div className="container">
          <Route exact path="/" component={Landing} />
          <Route path="/contact-us" component={ContactUs} />
        </div>
      </div>
    </BrowserRouter>
  </div>
);

export default App;
