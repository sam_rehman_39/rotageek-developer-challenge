import React from "react";
import Form, { IFormValues } from "./Form";
import { connect } from "react-redux";
import { submitForm } from "../../actions/formActions";

interface IDispatchProps {
  submitForm(values: IFormValues): void;
}

interface IStateProps {
  isFetching: boolean;
  error: boolean;
}

type IProps = IDispatchProps & IStateProps;

type IState = {
  message: string;
};

class ContactUs extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = { message: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidUpdate(prevProps: IProps) {
    const { isFetching, error } = this.props;
    if (prevProps.isFetching && !isFetching) {
      if (error) {
        this.setState(() => ({
          message: "An error occurred, please try again later."
        }));
      } else {
        this.setState(() => ({
          message:
            "Your details have been submitted. We will be in touch shortly."
        }));
      }
    }
  }
  handleSubmit(values: IFormValues) {
    const { submitForm } = this.props;
    submitForm(values);
  }
  render() {
    const { message } = this.state;
    const { error } = this.props;
    const formLabel =
      "Please fill out the form below with your details and message and we will be in touch shortly.";
    return (
      <div className="mt-3 mt-md-4 mb-3 mb-md-4">
        <h4 className="d-block d-md-none">{formLabel}</h4>
        <h2 className="d-none d-md-block">{formLabel}</h2>
        <Form onSubmit={this.handleSubmit} />
        {message && (
          <div className="text-center">
            <span className={error ? "text-danger" : "text-success"}>
              {message}
            </span>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  isFetching: state.form.isFetching,
  error: state.form.error
});

export default connect<IStateProps, IDispatchProps>(
  mapStateToProps,
  { submitForm }
)(ContactUs);
