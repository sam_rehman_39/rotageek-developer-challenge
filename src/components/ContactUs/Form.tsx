import React from "react";
import { withFormik, FormikProps, Form, Field } from "formik";
import * as Yup from "yup";

interface IProps {
  onSubmit(x: {}): void;
}

export interface IFormValues {
  name: string;
  email: string;
  tel: string;
  message: string;
}

const InnerForm = (props: IProps & FormikProps<IFormValues>) => {
  const { errors, touched } = props;

  return (
    <Form>
      <div className="m-3 m-md-4">
        <label>Tell us your name.</label>
        <Field
          type="text"
          name="name"
          placeholder="Name"
          className="form-control"
        />
        {touched.name && errors.name && (
          <span className="text-danger small">{errors.name}</span>
        )}
      </div>
      <div className="m-3 m-md-4">
        <label>What's your email address?</label>
        <Field
          type="email"
          name="email"
          placeholder="Email"
          className="form-control"
        />
        {touched.email && errors.email && (
          <span className="text-danger small">{errors.email}</span>
        )}
      </div>
      <div className="m-3 m-md-4">
        <label>What's your phone number?</label>
        <Field
          type="tel"
          name="tel"
          placeholder="Phone"
          className="form-control"
        />
        <span />
        {touched.tel && errors.tel && (
          <span className="text-danger small">{errors.tel}</span>
        )}
      </div>
      <div className="m-3 m-md-4">
        <label>Please type in your message below.</label>
        <Field
          component="textarea"
          name="message"
          placeholder="Type your message here"
          className="form-control"
        />
        <span />
        {touched.message && errors.message && (
          <span className="text-danger small">{errors.message}</span>
        )}
      </div>
      <div className="text-center pb-3">
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </div>
    </Form>
  );
};

const ContactUsForm = withFormik<IProps, IFormValues>({
  mapPropsToValues: () => {
    return {
      name: "",
      email: "",
      tel: "",
      message: ""
    };
  },
  validationSchema: Yup.object().shape({
    name: Yup.string().required("Name is required"),
    email: Yup.string()
      .email("Please enter a valid email")
      .required("Email is required"),
    tel: Yup.string().required("Phone number is required"),
    message: Yup.string().required("Message is required")
  }),
  handleSubmit: (values, { props, resetForm }) => {
    resetForm();
    props.onSubmit(values);
  }
})(InnerForm);

export default ContactUsForm;
