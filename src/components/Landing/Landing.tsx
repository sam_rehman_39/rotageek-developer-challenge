import React from "react";
import { Link } from "react-router-dom";

import "./landing.scss";

const Landing = () => (
  <div className="landing-container">
    <div className="text-center">
      <div>
        <span className="h5 text-secondary">
          Hello and welcome to this demo created by{" "}
          <span className="font-weight-bold">Sami Ur Rehman</span>. This is a
          basic "Contact Us" page demo. Please click on the button below to get
          started. Please click on the "Company Name" logo on the top left hand
          corner to navigate back to this page at any time.
        </span>
      </div>
      <Link to="/contact-us" className="btn btn-primary mt-3">
        Contact Us
      </Link>
    </div>
  </div>
);

export default Landing;
