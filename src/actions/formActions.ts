import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  SUBMITTING_FORM,
  SUBMIT_FORM_SUCCESS,
  SUBMIT_FORM_ERROR
} from "./types";
import { IFormValues } from "../components/ContactUs/Form";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";

const mock = new MockAdapter(axios);

export const submitForm = (values: IFormValues) => async (
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) => {
  dispatch({
    type: SUBMITTING_FORM,
    payload: {
      isFetching: true,
      error: false,
      values: { name: "", email: "", phone: "", message: "" }
    }
  });
  try {
    await axios.post("/submit");
    const formValues = localStorage.getItem("form-submissions");
    const formValuesJson = formValues ? JSON.parse(formValues) : null;
    if (formValuesJson) {
      localStorage.setItem(
        "form-submissions",
        JSON.stringify([...formValuesJson, values])
      );
    } else {
      localStorage.setItem("form-submissions", JSON.stringify([values]));
    }
    dispatch({
      type: SUBMIT_FORM_SUCCESS,
      payload: { isFetching: false, error: false, values }
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: SUBMIT_FORM_ERROR,
      payload: { isFetching: false, error: true }
    });
  }
};

mock
  .onPost("/submit")
  .replyOnce(200)
  .onPost("/submit")
  .replyOnce(500)
  .onPost("/submit")
  .reply(200);
