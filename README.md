# Rotageek Developer Challenge

Hello and welcome to this demo created by Sami Ur Rehman. This is a basic "Contact Us" page demo.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Clone the Repository

### `git clone git@bitbucket.org:sam_rehman_39/rotageek-developer-challenge.git`

### Change to the rotageek-developer-challenge Directory

### `cd rotageek-developer-challenge`

### Install the Dependencies

### `npm install`

## Run the Demo

### `npm start`

And that is it! You should now be able to view it in your browser.

## Built With

[React](https://reactjs.org)

[Redux](https://redux.js.org/)

[TypeScript](https://www.typescriptlang.org/)

Some common dependencies that I have made use of include:

[Formik](https://github.com/jaredpalmer/formik)

[Bootstrap](https://getbootstrap.com)

## Code Decisions

This demo could still be improved and is by no means perfect. I made use of [axios-mock-adapter](https://github.com/ctimmerm/axios-mock-adapter) to mock API calls. The way it is set up is that first form submission is a success, second one is a failure and all subsequent requests are successful. All successful form submissions are stored in local storage of the browser. In order to see all submissions, please run this command in the console of your browser:

`JSON.parse(localStorage.getItem('form-submissions'))`

I had some time constraints so I focused on some things more than the others. These are the things I would have done if I had more time:

- Made less use of bootstrap and more of SCSS to improve the overall look and feel of the application to provide a better user experience.

- Written unit tests. I understand that testing is really important.

## Conclusion

Lastly, I hope you like this demo. I would love to get some feedback on what you think of it and how it can be improved :)
